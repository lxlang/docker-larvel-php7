FROM lxlang/php7

MAINTAINER tobiaslang86@googlemail.com

LABEL Description="Laravel PHP7 Base Container"

# # # # # # # # # # # # # # # # # # #  set timezone to Europe/Berlin
RUN ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

## Install curl.
RUN apt-get update && \ 
    apt-get install -y curl 

## install nodejs and gulp 
RUN curl -sL https://deb.nodesource.com/setup | sudo bash - && \
    apt-get install -y nodejs && \
    npm install -g npm

## cleanup junkfiles
RUN apt-get purge -y curl && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

## add apache basic config                                        
ADD ./assets/laravel.conf /etc/apache2/sites-available/laravel.conf

RUN a2dissite 000-default
RUN a2ensite laravel
RUN a2enmod proxy
RUN a2enmod rewrite

## Install apache, PHP, and supplimentary programs. curl and lynx-cur are for debugging the container. 
WORKDIR /var/www

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]